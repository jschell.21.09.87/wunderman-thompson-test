const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const cssComb = require('gulp-csscomb');
const cmq = require('gulp-merge-media-queries');
const cleanCss = require('gulp-clean-css');
const browserify = require('gulp-browserify');
const uglify = require('gulp-uglify');
const minifyHtml = require('gulp-minify-html');
const babel = require('gulp-babel')
const concat = require('gulp-concat');
const image = require('gulp-image');

gulp.task('sass',function(){
    gulp.src(['src/assets/css/**/*.scss'], {allowEmpty : true})
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(cssComb())
        .pipe(cmq({log:true}))
        .pipe(concat('main.css'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cleanCss())
        .pipe(gulp.dest('dist/assets/css'))
        .pipe(reload())
});

gulp.task('js',function(){
    gulp.src(['src/assets/js/**/*.js'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(concat('main.js'))
        .pipe(browserify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(gulp.dest('dist/assets/js'))
        .pipe(reload())
});

gulp.task('html',function(){
    gulp.src(['src/**/*.html'])
        .pipe(plumber({
            handleError: function (err) {
                console.log(err);
                this.emit('end');
            }
        }))        
        .pipe(minifyHtml())
        .pipe(gulp.dest('dist'))
        .pipe(reload())
});

// gulp.task('image',function(){
//     return gulp.src("src/assets/images/**/*.{jpg,jpeg,png}")
//         .pipe(webp())
//         .pipe(gulp.dest("dist/assets/images"));
// });

gulp.task('image', function () {
    gulp.src('src/assets/images/**/*.{jpg,jpeg,png}')
        .pipe(image())
        .pipe(gulp.dest('dist/assets/images'));
});

gulp.task('default',function(){
    browserSync.init({
        server: ["./src", "./dist"]
    });
    gulp.watch('src/assets/js/**/*.js',gulp.series('js'));
    gulp.watch('src/assets/css/**/*.scss',gulp.series('sass'));
    gulp.watch('src/**/*.html',gulp.series('html'));
    gulp.watch('src/assets/images/**/*',gulp.series('image'));
});
