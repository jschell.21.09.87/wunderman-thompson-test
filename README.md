# Wunderman Thompson Test
#### Jonathan Schell

## Intro
En éste README doy las pautas para realizar la instalación del proyecto, dentro del mismo se encontrará documentación detallada.

## Instalación

Clonar el repositorio y acceder a la carpeta del proyecto clonado.

```bash
git clone https://gitlab.com/jschell.21.09.87/wunderman-thompson-test.git
cd wunderman-thompson-test
```
Dentro del directorio del proyecto instalar las dependencias (el proyecto se realizó con npm 6.14.5 - node v14.3.0).

```bash
npm install
```
Una vez finalizada la instalación del proyecto simplemente corremos el mismo.

```bash
npm start
```