const errorMessages = {
    empty : 'El campo no puede quedar vacío.',
    moreTwo : 'El campo debe contener al menos 2 caracteres.',
    email : 'El formato de email es inválido.',
    ci : 'El formato de CI es inválido.',
    bases : 'Necesitas aceptar las bases y condiciones para continuar.',
}

module.exports = {
    errorMessages
}