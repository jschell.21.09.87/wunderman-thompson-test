const slider = require('./slider')
const datos = require('../../data/datos')
const errorMessages = require('./errorMessages')


class Project{

    constructor(){
        this.inputs = document.querySelectorAll('.input-group');
        this.data = datos
        this.errorMessages = errorMessages.errorMessages
        this.canSubmit = true
    }

    initSlider(){
        slider.slider()        
    }

    // Interaction with inputs
    resetActiveInAllInputsIfNotValue(){
        for (let index = 0; index < this.inputs.length; index++) {

            const element = this.inputs[index]

            for (let i = 0; i < element.children.length; i++) {
                const elementInput = element.children[i];
                if(elementInput.localName === 'input'){                    
                    if(elementInput.value === ''){
                        element.classList.remove('active')
                    }
                }
            }     
            
        }
    }

    onFocusInputActiveField(e){        
        const {keyCode} = e
        
        const elFocused = document.querySelector('.input-group input:focus')

        if(elFocused){
            // first, remove all actives
            this.resetActiveInAllInputsIfNotValue()
            elFocused.parentNode.classList.add('active')
        }
    }

    focusInputs($target){
        this.resetActiveInAllInputsIfNotValue()     

        const {target} = $target        

        if(target.classList.contains('input-group')){
            target.classList.add('active')
            this.removeAllErrors(target)
        }else{
            target.parentNode.classList.add('active')
            this.removeAllErrors(target.parentNode)
        }
    }

    // handle data
    makeOptionTag(name){
        const content = document.createTextNode(name)

        const el = document.createElement("option")

        el.value = name

        el.appendChild(content)

        return el
    }

    insertDataInDepartament(){
        const {dptosLocs} = this.data

        // get keys
        const keys = Object.keys(dptosLocs)

        const select = document.querySelector('select[name="departament"]')
        const optionHelp = document.querySelector('select[name="departament"] option')

        for (let index = 0; index < keys.length; index++) {    
            const name = keys[index];
            
            // make option element
            const el = this.makeOptionTag(name)

            select.insertBefore(el, optionHelp)
        }
    }

    insertDataInLocation(departament){
        // reset options
        this.resetDataInLocation()
        
        const {dptosLocs} = this.data

        const keys =  dptosLocs[departament]

        const select = document.querySelector('select[name="location"]')
        const optionHelp = document.querySelector('select[name="location"] option')

        for (let index = 0; index < keys.length; index++) {    
            const name = keys[index];
            
            // make option element
            const el = this.makeOptionTag(name)

            select.insertBefore(el, optionHelp)
        }
    }

    resetDataInLocation(){
        const options = document.querySelectorAll('select[name="location"] option')

        for (let index = 0; index < options.length; index++) {
            const element = options[index];
            
            if(element.value !== ''){
                element.remove()
            }
        }
    }

    // form validation
    addClickToSubmit(){
        const submit = document.querySelector('#submit')

        submit.addEventListener('click', e => {
            this.checkEmptyValues()
            this.checkAcceptedBases()
            this.canSubmit && alert('Formulario enviado con éxito!')
        })
    }

    checkEmptyValues(){                   
        for (let index = 0; index < this.inputs.length; index++) {
            const element = this.inputs[index];            

            // reset all errors
            this.removeAllErrors(element)

            for (let i = 0; i < element.children.length; i++) {
                const input = element.children[i];

                if((input.localName === 'input') || (input.localName === 'select')){

                    if(input.value === ''){                        
                        input.parentNode.nextElementSibling.children[0].innerHTML = this.errorMessages.empty
                        this.paintErrorBorderInField(input.parentNode)
                        this.canSubmit = false
                    }else{
                        if((input.name === 'nombre') || (input.name === 'apellido')){                            
                            this.checkTwoCharacters(input)
                        }else if(input.name === 'email'){
                            this.checkEmailField(input)
                        }else if(input.name === 'ci'){
                            this.checkCI(input)
                        }
                    }

                }
            }
        }
    }

    checkTwoCharacters(inputEl){        
        if(inputEl.value.length < 2){            
            inputEl.parentNode.nextElementSibling.children[0].innerHTML = this.errorMessages.moreTwo
            this.paintErrorBorderInField(inputEl.parentNode)
            this.canSubmit = false
        }else{
            this.removeErrorBorderInField(inputEl.parentNode)
        }
    }

    checkEmailField(inputEl){
        const reGex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        if(!reGex.test(String(inputEl.value).toLowerCase())){
            inputEl.parentNode.nextElementSibling.children[0].innerHTML = this.errorMessages.email
            this.paintErrorBorderInField(inputEl.parentNode)
            this.canSubmit = false
        }else{
            this.removeErrorBorderInField(inputEl.parentNode)  
        }
    }

    checkCI(inputEl){        
        const ci = inputEl.value
        var arrCoefs = new Array(2, 9, 8, 7, 6, 3, 4, 1);
        var suma = 0;
        //Para el caso en el que la CI tiene menos de 8 digitos
        //calculo cuantos coeficientes no voy a usar
        var difCoef = parseInt(arrCoefs.length - ci.length);
        //recorro cada digito empezando por el de más a la derecha
        //o sea, el digito verificador, el que tiene indice mayor en el array
        for (var i = ci.length - 1; i > -1; i--) {
            //Obtengo el digito correspondiente de la ci recibida
            var dig = ci.substring(i, i + 1);
            //Lo tenía como caracter, lo transformo a int para poder operar
            var digInt = parseInt(dig);
            //Obtengo el coeficiente correspondiente al ésta posición del digito
            var coef = arrCoefs[i + difCoef];
            //Multiplico dígito por coeficiente y lo acumulo a la suma total
            suma = suma + digInt * coef;
        }
        // si la suma es múltiplo de 10 es una ci válida
        if ((suma % 10) === 0) {
            this.removeErrorBorderInField(inputEl.parentNode)            
        }else{
            inputEl.parentNode.nextElementSibling.children[0].innerHTML = this.errorMessages.ci
            this.paintErrorBorderInField(inputEl.parentNode)
            this.canSubmit = false
        }
    }

    checkKeyNumber(){
        const input = document.querySelector('input[name="ci"]')

        input.addEventListener('keypress', key => {            
            !Number.isInteger(parseInt(key.key)) && key.preventDefault()
        })
    }

    checkAcceptedBases(){
        const input = document.querySelector('input[name="bases"]')

        if(!input.checked){
            input.parentNode.nextElementSibling.children[0].innerHTML = this.errorMessages.bases

            this.canSubmit = false
        }else{
            input.parentNode.nextElementSibling.children[0].innerHTML = ''
        }
    }

    paintErrorBorderInField(inputDiv){
        inputDiv.style = 'border: solid 1px red'
    }

    removeErrorBorderInField(inputDiv){
        inputDiv.removeAttribute('style')
    }

    removeAllErrors(inputDiv){
        if(inputDiv.classList.contains('input-group')){

            inputDiv.nextElementSibling.children[0].innerHTML = ''

            this.removeErrorBorderInField(inputDiv)
        }   

        this.canSubmit = true
    }

}

const start = () => {
    const project = new Project()

    // init slider
    project.initSlider()

    // get data for departament
    project.insertDataInDepartament()

    // init submit function
    project.addClickToSubmit()

    // init control keypress in CI
    project.checkKeyNumber()

    // init focus active on inputs
    window.addEventListener('click', $target => {
        project.focusInputs($target)
    })

    // need detect focus in input from tab button
    window.addEventListener('keyup', $target => {
        project.onFocusInputActiveField($target)
    })

    // get data for departament
    const elDepartament =  document.querySelector('select[name="departament"]')
    const elLocation =  document.querySelector('select[name="location"]')

    elDepartament.addEventListener('change', e => {
        const {value} = e.target

        project.insertDataInLocation(value)

        elLocation.removeAttribute('disabled')
    })
}

// start all scripts
document.body.onload = start()