// Slick Slider depende de jQuery por lo que necesitaré importarlo para ello.
const slider = () => {
    $('#slider').slick({
        infinite : true        
    })
}

module.exports = {
    slider
}